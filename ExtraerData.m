
% Bio-PC
%addpath('home/blancag/BlancaG/fMRI Databases/PBAIC2007 Database/Brain Image Data/pghdata(s13) preprocessed/subject01/preprocessed/anz/vr01')
%addpath('/home/blancag/BlancaG/MATLAB/Sparselab/SparseLab21-Core/SparseLab2.1-Core/Solvers')

addpath('/home/blancag/BlancaG/fMRI Databases/PBAIC2007 Database/Brain Image Data/subject13 preproceessed matlab/subject13/preprocessed/mat_v6/vr01')
load('subject13_run01_v6')


% Data funcional
fmri=subject13_run01.braindata.fmridata;
v1=fmri(:,:,:,1);
figure(3), imshow(v1(:,:,10),[])

% Data funcional ciclo
fmri_cile=subject13_run01.braindata.fmridata;
figure(3)
for z=1:34
    imshow(fmri_cile(:,:,z,1),[])
    pause
end


%Data Mask
mask=subject13_run01.braindata.mask;
for z=1:34
    imshow(mask(:,:,z))
    pause
end


% Data estructural
anatom=subject13_run01.braindata.structuraldata;
figure(2)
for z=1:5:224
    imshow(anatom(:,:,z),[])
    pause
end

% Data estructural corregida
anatom_coreg=subject13_run01.braindata.structuraldata_coreg;
figure(4)
for z=1:2:162
    imshow(anatom_coreg(:,:,z),[])
    pause
end

%Feature


%% 

% Extraer con formato analyze

filename=sprintf('subject01_vr01_func_00696')
info = analyze75info(filename);
vol=analyze75read(info);
figure(4), imshow(vol(:,:,15))

%% 
addpath('/home/blancag/BlancaG/fMRI Databases/PBAIC2007 Database/Brain Image Data/pghdata(s01) preprocessed/subject01/preprocessed/anz/vr01')
% Extraer con formato analyze nuevo
m=zeros(704,139264);
for i=1:704
    nom_archivo='subject01_vr01_func_00';
    if i<10
        archivo=strcat(nom_archivo,'00',num2str(i));
    else if i>9 && i<100
            archivo=strcat(nom_archivo,'0',num2str(i));
        else if i>99
                archivo=strcat(nom_archivo,num2str(i));
            end
        end
    end           
    filename=sprintf(archivo);
    info = analyze75info(filename);
    vol=analyze75read(info);
    fila=vol(:);    
    %m(i,:)=vol(:);
    
    for j=1:length(fila)
        m(i,j)=fila(j);
    end
    
end    
    
%figure(4), imshow(vol(:,:,15))

%% Mask
%
% addpath('/home/blancag/BlancaG/fMRI Databases/PBAIC2007 Database/Brain Image Data/pghdata(s01) preprocessed/subject01/preprocessed/anz/mask')
addpath('/home/blancag/BlancaG/fMRI Databases/PBAIC2007 Database/Brain Image Data/pghdata(s01) preprocessed/subject01/preprocessed/anz/mask')

%para el sujeto13
%addpath('/home/blancag/BlancaG/fMRI Databases/PBAIC2007 Database/Brain Image Data/subject13 preproceessed matlab/subject13/preprocessed/mat_v6/vr01')
%load('subject01_mask')

%se lee la data con analyze
filename=sprintf('subject01_mask');
mask_info = analyze75info(filename);  

%Data Mask
mask=analyze75read(mask_info);  
%para el sujeto 13
%mask=subject13_run01.braindata.mask;

%para saber la longitud de la matriz en este caso sera de 34
len = length(mask(1,1,:));
%longitud de las columnas o filas que son de 64x64 entonces sera 64
len64 = length(mask(:,1,1));
%array que seran de todos los valores de la mask que sean 1 sera guardado en el array
all_mask=find(mask(:));
%array que guardara cuantos valores fueron 1 en cada rebanada
len_reb=zeros(len,1,'double')
%array que obtendra la longitud final del array ALL_MASK en paralelo por cada
%rebanada
ini_reb=zeros(len,1,'double')
for i=1:length(mask(1,1,:))
    %guardando las longitudes de cada rebanada
    len_reb(i)= length(find(mask(:,:,i)));
    ini_reb(i)=sum(len_reb);
end

%para encontrar las cordenadas de cada valor de la matriz ALL_MASK
%seria hasta length(all_mask) en este caso lo trabajaremos hasta 20 como
%prueba para estudiar los 20 primeros valores

%length(all_mask)
 for i=1:20
    
    %saco el mod (modulo de la division)
    %con el fin de saber cuando este en el borde de abajo de la matriz
    % ya que si esta en el borde de abajo de la matriz x toma el valor de
    % la seguiente columna (estando en 2, pero el valor que tomara es el 3)
    % y si eso sucede no le sumamos el +1 
    if (mod(all_mask(i),len64)==0)
        xx = floor(all_mask(i)/len64);
    else
        xx = floor(all_mask(i)/len64+1);  
    end
    % cuando estamos comenzando el ciclo en el dorde izquierdo de la matriz
    % no debemos restarle a x -1 porque no nos daria la coordenada en y
    if (xx == 0)
        yy = all_mask(i)-(len64*(xx));
    else
        yy = all_mask(i)-(len64*(xx-1));
    end
    fprintf('valor: %0.0f ',all_mask(i));
    fprintf('x: %0.0f',xx);
    fprintf(' y: %0.0f\n',yy);
 end
